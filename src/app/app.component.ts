import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionService } from './services/notificacion.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app-practice';

  constructor(private router: Router, private notifyService: NotificacionService) { }


  ngOnInit(): void {
    const navbar = document.querySelector('app-nav-bar');
    const bar = document.querySelector('#bar');
    const side = document.querySelector('#dashboard-menu');
    const notfs = document.querySelector('#dashboard-notifications');
    const bell = document.querySelector('#bell');

    this.obtenerCategorias();

    bar.addEventListener('click', ({ currentTarget }) => {
      const i = String(side.getAttribute('style'));
      if (i === 'opacity: 0; visibility: hidden;'){
        side.setAttribute('style', 'visibility: visible; opacity: 1;');
        notfs.setAttribute('style', 'opacity: 0; visibility: hidden;');
      }else{
        side.setAttribute('style', 'opacity: 0; visibility: hidden;');
      }
    });

    bell.addEventListener('click', ({ currentTarget }) => {
      const i = String(notfs.getAttribute('style'));
      if (i === 'opacity: 0; visibility: hidden;'){
        notfs.setAttribute('style', 'visibility: visible; opacity: 1;');
        side.setAttribute('style', 'opacity: 0; visibility: hidden;');
      }else{
        notfs.setAttribute('style', 'opacity: 0; visibility: hidden;');
      }
    });
    if (!sessionStorage.getItem('usuario')){
      navbar.setAttribute('style', 'visibility: hidden;');
      this.router.navigate(['login']);
    }else{
      navbar.setAttribute('style', 'visibility: visible;');
      this.router.navigate(['dashboard']);
    }
  }

  obtenerCategorias(): void{
    this.notifyService.obtenerCategorias().subscribe( data => {
      if (data != null){
      sessionStorage.categorias = JSON.stringify(data);
      }else{
        console.log('Error al obtener categorias!!');
      }
    });
  }
}

