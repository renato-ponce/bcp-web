import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionService } from '../services/notificacion.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  public datos: string;
  public usuario: JSON = JSON.parse(sessionStorage.getItem('usuario'));
  public noLeidas: number;

  constructor(private router: Router, private notifyService: NotificacionService) { }

  ngOnInit(): void {
    console.log(this.usuario);
    if (this.usuario != null){
      this.datos = String(this.usuario['nombre'] + ' ' + this.usuario['apellido']);
      setInterval(() => {
        this.noLeidas = Number(sessionStorage.getItem('noLeidas'));
      }, 1000);
    }else{
      this.datos = 'Usuario';
    }
  }
  cerrarSesion(): void{
    sessionStorage.clear();
    window.location.reload();
    this.router.navigate(['login']);
  }
}
