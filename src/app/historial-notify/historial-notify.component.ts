import { Component, OnInit } from '@angular/core';
import { NotificacionService } from '../services/notificacion.service';
import { ListaNoti } from '../model/ListaNoti';

@Component({
  selector: 'app-historial-notify',
  templateUrl: './historial-notify.component.html',
  styleUrls: ['./historial-notify.component.css']
})
export class HistorialNotifyComponent implements OnInit {
  lista: [] = [];
  total: number;
  listaEliminar: ListaNoti = {
    listaNotUsuIds: [0]
  };

  public usuario: JSON;

  constructor(
    private notifyService: NotificacionService
    ) {
    }

  ngOnInit(): void {
    if (sessionStorage.getItem('usuario') != null){

    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    const idUsu = this.usuario['idUsuario'];

    this.getNotificacionesxUsuario(idUsu);
    }
  }

  getNotificacionesxUsuario(idUsu): void{
    this.notifyService.getNotificacionesPorUsuario(0, idUsu).subscribe(resp => {
      this.lista = resp['listaNotificaciones'];
      this.total = this.lista.length - resp['cantidadNoLeidas'];
      });
  }

  recopilarNotificaciones(idNoti: number): void{
    if (this.listaEliminar.listaNotUsuIds.includes(idNoti) === true) {

      const index = this.listaEliminar.listaNotUsuIds.indexOf(idNoti);

      this.listaEliminar.listaNotUsuIds.splice(index, index);
    }else{
      this.listaEliminar.listaNotUsuIds.push(idNoti);
    }
    console.log(this.listaEliminar.listaNotUsuIds.join('|'));
  }


  eliminarNotificaciones(): void{
    if (this.listaEliminar.listaNotUsuIds.length <= 1 ){
      alert('Seleccione las notificaciones a eliminar');
    }else{
        this.notifyService.eliminarNotificaciones(this.listaEliminar).subscribe(resp => {
          alert(resp);
          this.getNotificacionesxUsuario(this.usuario['idUsuario']);
        });
    }
  }

  getEstado(estado): boolean{
    if (estado === true){
      return true;
    }else{
      return false;
    }
  }
}
