import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialNotifyComponent } from './historial-notify.component';

describe('HistorialNotifyComponent', () => {
  let component: HistorialNotifyComponent;
  let fixture: ComponentFixture<HistorialNotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialNotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
