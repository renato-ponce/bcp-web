import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  tarjeta: number;

  constructor() { }

  ngOnInit(): void {
    const dashboard = document.querySelector('app-dashboard');

    dashboard.setAttribute('style', 'width: 100%;');
    var usuario = JSON.parse(sessionStorage.getItem('usuario'));

    this.tarjeta = usuario['tarjeta'];
  }

}
