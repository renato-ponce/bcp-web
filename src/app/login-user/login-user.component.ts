import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})

export class LoginUserComponent implements OnInit {
  card: string;
  clave: string;

  constructor(private authService: UsuarioService, private router: Router) {
    this.card = '';
    this.clave = '';
   }

  ngOnInit(): void {
    //const login = document.querySelector('app-login-user');
    //login.setAttribute('style', 'width:80%; margin:10%;');

    // const login = document.querySelector('app-login-user');
    const login = document.getElementsByTagName('app-login-user')[0];
    // login.set('style', 'width:100%');
    login.setAttribute('style', 'width:100%');
    const bodyContainer = document.getElementById('body-container');
    bodyContainer.setAttribute('style', 'width: 100%; position: fixed; top: 0px;');
  }

  login(): void{
    console.log(this.card);
    console.log(this.clave);
    const user = {tarjeta: this.card, password: this.clave};

    this.authService.login(user).subscribe( data => {
      console.log(data);
      if (data == null){
        alert('Tarjeta o clave incorrectas');
      }else{
      sessionStorage.usuario = JSON.stringify(data);
      sessionStorage.password = this.clave;
      window.location.reload();
      this.router.navigate(['dashboard']);
      }
    });
 }
}
