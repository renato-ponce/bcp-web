import { JsonPipe } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Session } from 'protractor';
import { Observable } from 'rxjs';
import { Usuario } from '../model/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient) {
    console.log('Servicio User');
  }

  login(user: any): Observable<any>{
    const url = 'http://localhost:8080/api/usuario';
    return this.http.post<Session>(url, user);
  }

  updateUser(user: Usuario): Observable<any>{
    const url = 'http://localhost:8080/api/usuario/actualizarUsu';

    return this.http.post<any>(url, user);
  }
}
