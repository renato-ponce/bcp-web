import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificacionService {
  url = '';
   constructor(private http: HttpClient) {
    console.log('Servicio Notify');
   }

   // tslint:disable-next-line: typedef
   getNotificacionesPorUsuario(categoria: number, usuario: number) {

    this.url = 'http://localhost:8080/api/notificacion/notificacionUsu?idUsuario='.concat(String(usuario)).concat('&idCategoria='.concat(String(categoria)));

    const header = new HttpHeaders()
     .set('Type-content', 'aplication/json');

    return this.http.get(this.url, {
       responseType: 'json'
     });
   }

   // tslint:disable-next-line: typedef
   obtenerCategorias(){
    this.url = 'http://localhost:8080/api/notificacion/categorias';

    const header = new HttpHeaders()
     .set('Type-content', 'aplication/json');

    return this.http.get(this.url, {
       headers: header
     });
   }

   // tslint:disable-next-line: typedef
   enviarNotificacion(body){
    const url = 'http://localhost:8080/api/notificacion/enviarNot';

    return this.http.post<any>(url, body);
   }

   // tslint:disable-next-line: typedef
   marcarLeido(idUsuNot){
    const url = 'http://localhost:8080/api/notificacion/marcarLeido?idUsuarioNotificacion='.concat(idUsuNot);

    return this.http.get(url, {
      responseType: 'text'
    });
  }

  eliminarNotificaciones(lista){
    const url = 'http://localhost:8080/api/notificacion/eliminarNot';
    return this.http.post(url, lista, {responseType: 'text'});
  }
}
