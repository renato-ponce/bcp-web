import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UpdateUserComponent } from './update-user/update-user.component';
import { LoginUserComponent } from './login-user/login-user.component';

const appRoutes = [
  /*{ path: 'prueba', component: PruebaVenComponent,  pathMatch: 'full'},*/
  { path: 'dashboard', component: DashboardComponent,  pathMatch: 'full'},
  { path: 'update', component: UpdateUserComponent, pathMatch: 'full'},
  { path: 'login', component: LoginUserComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const routing = RouterModule.forRoot(appRoutes);
