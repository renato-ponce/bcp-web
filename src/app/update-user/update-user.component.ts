import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../services/usuario.service';
import { NotificacionService } from '../services/notificacion.service';
import { Usuario } from '../model/Usuario';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  public usuario: JSON = JSON.parse(sessionStorage.getItem('usuario'));

  newnombres: string;
  newapellidos: string;
  newemail: string;

  notificaciones = JSON.parse(sessionStorage.getItem("categorias"));

  notiSeguridad = this.notificaciones[2];

  nombres: string = this.usuario['nombre'];
  apellidos: string = this.usuario['apellido'];
  email: string = this.usuario['email'];
  id: number = this.usuario['idUsuario'];
  clave: number = Number(sessionStorage.getItem('password'));

  constructor(private userService: UsuarioService, private notifyService: NotificacionService) { }

  ngOnInit(): void {
    this.newnombres = this.nombres;
    this.newapellidos = this.apellidos;
    this.newemail = this.email;
    const doc = document.querySelector('app-update-user');
    doc.setAttribute('style', 'width: 100%;');
    const btnUpdate =  document.querySelector('#btnActualizar');

    btnUpdate.addEventListener('click', ({ currentTarget }) => {
      this.actualizar();
    });

  }

  enviarNotificacion(): void{
      const notify = {
        listaUsuarios: [this.id],
        idCategoria: this.notiSeguridad['idCategoria'],
        descripcionCategoria: this.notiSeguridad['descripcion'],
        idNotificacion: 3,
        descripcionNotificacion: 'Usted ha actualizado sus datos'
      };

      this.notifyService.enviarNotificacion(notify).subscribe(data =>{
        console.log(data);
      });
  }

  actualizar(): void {

    const user: Usuario =  {
      idUsuario: this.id,
      nombre: this.newnombres,
      apellido: this.newapellidos,
      email: this.newemail,
      password: this.clave,
      tarjeta: 0
    };

    console.log(user);

    this.userService.updateUser(user).subscribe( data => {
      if (data != null){
      sessionStorage.usuario = JSON.stringify(data);
      this.enviarNotificacion();
      alert('Datos Actualizados');
      window.location.reload();
      }else{
        alert('Error al actualizar');
      }
    });
  }

}
