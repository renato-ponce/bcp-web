import { Component, OnInit } from '@angular/core';
import { NotificacionService } from '../services/notificacion.service';

@Component({
  selector: 'app-side-notify',
  templateUrl: './side-notify.component.html',
  styleUrls: ['./side-notify.component.css']
})
export class SideNotifyComponent implements OnInit {
  lista: any = [];

  public usuario: JSON;

  constructor(
    private notifyService: NotificacionService
    ) {
    }

  ngOnInit(): void {
    if (sessionStorage.getItem('usuario') != null){

    this.usuario = JSON.parse(sessionStorage.getItem('usuario'));
    const idUsu = this.usuario['idUsuario'];

    this.getNotificacionesxUsuario(idUsu);

    setInterval(() => {
      this.getNotificacionesxUsuario(idUsu);
      }, 1000);
    }
  }

  getNotificacionesxUsuario(idUsu): void{
    this.notifyService.getNotificacionesPorUsuario(0, idUsu).subscribe(resp => {
      this.lista = resp['listaNotificaciones'];
      sessionStorage.setItem('noLeidas', resp['cantidadNoLeidas']);
      });
  }

  marcarLeido(id): void{
    this.notifyService.marcarLeido(id).subscribe(resp => {
      alert(resp);
      window.location.reload();
    });
  }

  getEstado(estado): boolean{
    if (estado === true){
      return true;
    }else{
      return false;
    }
  }
}
