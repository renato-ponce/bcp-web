import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideNotifyComponent } from './side-notify.component';

describe('SideNotifyComponent', () => {
  let component: SideNotifyComponent;
  let fixture: ComponentFixture<SideNotifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideNotifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
